package com.example.jitsitest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.facebook.react.modules.core.PermissionListener
import org.jitsi.meet.sdk.*
import java.net.MalformedURLException
import java.net.URL

class MainActivity : AppCompatActivity(), JitsiMeetActivityInterface {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Initialize default options for Jitsi Meet conferences.
        val serverURL: URL
        serverURL = try {
            URL("https://vchatin.palapabeta.com")
        } catch (e: MalformedURLException) {
            e.printStackTrace()
            throw RuntimeException("Invalid server URL!")
        }

        val userInfo = JitsiMeetUserInfo()
        userInfo.displayName = "Yolo"
        val defaultOptions = JitsiMeetConferenceOptions.Builder()
            .setServerURL(serverURL)
            .setUserInfo(userInfo)
            .setWelcomePageEnabled(false)
            .setFeatureFlag("invite.enabled", false)
            .setFeatureFlag("pip.enabled", false)
            .setFeatureFlag("meeting-name.enabled", true)
            .setFeatureFlag("toolbox.alwaysVisible", true)
            .setFeatureFlag("raise-hand.enabled", false)
            .setFeatureFlag("add-people.enabled", false)
            .build()

        /*val jitsiView = JitsiMeetView(this)
        jitsiView.join(defaultOptions)*/
        JitsiMeet.setDefaultConferenceOptions(defaultOptions)
    }

    fun onButtonClick(v: View?) {
        val editText = findViewById<EditText>(R.id.conferenceName)
        val text = editText.text.toString()
        if (text.length > 0) {
            // Build options object for joining the conference. The SDK will merge the default
            // one we set earlier and this one when joining.
            val options = JitsiMeetConferenceOptions.Builder()
                .setRoom(text)
                .build()
            // Launch the new activity with the given options. The launch() method takes care
            // of creating the required Intent and passing the options.
            JitsiMeetActivity.launch(this, options)
        }
    }

    override fun requestPermissions(p0: Array<out String>?, p1: Int, p2: PermissionListener?) {

    }
}